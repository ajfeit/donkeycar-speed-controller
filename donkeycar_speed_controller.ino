#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include "quaternionFilters.h"
#include "MPU9250.h"
#include "utility_classes.h"

#define I2Cclock 400000
#define I2Cport Wire
#define MPU9250_ADDRESS MPU9250_ADDRESS_AD0
// servo ranges
// To Do: update to be adjustable via command from the raspberry pi
#define THR_PWM_MIN  370 // this is the 'minimum' pulse length frame (out of 4096)
#define THR_PWM_MAX  420 // this is the 'maximum' pulse length frame (out of 4096)
#define STR_PWM_MIN  315 // this is the 'minimum' pulse length frame (out of 4096)
#define STR_PWM_MAX  435 // this is the 'maximum' pulse length frame (out of 4096)
#define ANALOG_IN_PIN A0
#define DIGITAL_IN_PIN 2
#define MAX_SPEED_PULSE 1000000

// global class instances
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
MPU9250 myIMU(MPU9250_ADDRESS, I2Cport, I2Cclock);
LowPassFilter thr_lf(0.1, 0.05);  // throttle low pass filter
LowPassFilter str_lf(0.1, 0.05);  // steering low pass filter

// global variables
unsigned long time;
unsigned long cmd_since;
unsigned int frame;
byte pi_addr = 0x13;
byte b1;
byte b2;
byte b3;
byte cmd[3] = {0, 0, 100};
int in1;
int in2;
int pwm1;
int pwm2;

// speed sensor variables
int sensorValue = 0;
int testValue = 0;
double outputValue = 0.0;
double pulseSeconds = 0.0;
double speed = 0.0;
double speedLast = 0.0;
volatile unsigned long lastPulse = 0;
volatile unsigned long lastPulseInterval = MAX_SPEED_PULSE;
volatile unsigned long currentPulse = 0;

void speedPulse() {
  currentPulse = micros();
  lastPulseInterval = (currentPulse - lastPulse);
  lastPulse = currentPulse;
}

// get steering and throttle cammands
// via i2c. Not currently used.
void geti2cCmd() {
  Wire.beginTransmission(pi_addr);
   Wire.write(byte('t'));
   Wire.requestFrom(pi_addr, 2);    // request 6 bytes from slave device #13
   while (Wire.available()) { // slave may send less than requested
     char c = Wire.read(); // receive a byte as character
     Serial.print(c);         // print the character
     // to do: convert recieved bytes into speed/steer commands
   }
   Wire.endTransmission();
}

// read serial control commands. Accepts
// either a control command, or a setting command,
// that could be used to adjust servo limits, 
// filter bandwidth, or feedback gains.
void getSerialCmd(byte (& cmd)[3]) {
  // code to read serial commands
  if (Serial.available() >= 4) {
    int n = Serial.readBytesUntil(byte(255), cmd, 4);
    if (n >= 3) {
      // control command
      if (char(cmd[0])=='c') {
        in1 = int(cmd[1]);
        in2 = int(cmd[2]);
        cmd_since = 0;
      }
    }
    // setting command. cmd[1] will specify what
    // setting, and cmd[2] will be the value.
    if (char(cmd[0])=='s') {
      Serial.print("got settings: ");
      Serial.print(cmd[1], DEC);
      Serial.print(" , ");
      Serial.println(cmd[2], DEC);
    }
  }
  while (Serial.available() > 0) {
    Serial.read();
  }
  if (cmd_since > 200) {
    //cmd[0] = 0;
    //cmd[1] = 0;
    //cmd[2] = 0;
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("Donkeycar Motor Controller");

  // setup i2c reciever
  Wire.begin();

  // setup servos
  Serial.println(" Initializing PWM");
  /*
  pwm.begin();
  pwm.setPWMFreq(60);  // Analog servos run at ~60 Hz updates
  delay(10);
  */

  Serial.println(" Initializing MPU-9250");
  // Test connection to MPU-9250
  // Read the WHO_AM_I register, this is a good test of communication
  /*
  byte c = myIMU.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);
  Serial.print(F("MPU9250 I AM 0x"));
  Serial.print(c, HEX);
  Serial.print(F(" I should be 0x"));
  Serial.println(0x71, HEX);
  */

  cmd_since = 0;
  //initialize filters
  str_lf.init();
  thr_lf.init();
  Serial.print("filter alpha: ");
  Serial.println(str_lf.get_alpha());

  //setup speed sensor interrupt
  Serial.println("Attaching speed sensor interrupt");
  attachInterrupt(digitalPinToInterrupt(DIGITAL_IN_PIN), speedPulse, RISING);
  
  Serial.println("Running...");
}

void loop() {
  // timed loop, 100Hz.
  // 10 frames per loop
  time = micros();
  frame++;  // the current frame
  if (frame >= 10) { frame = 0; }
  // frames

  switch(frame) {
    case 0:
      debug_putput();
      get_commands();
      // get_accel_data();
      break;
  
    case 1:
      map_throttle();
      // get_accel_data();
      break;
  
    case 2:
      // get_accel_data();
      send_commands();
      break;

    case 3:
      // get_accel_data();
      get_speed_data();
      break;

    case 4:
      // get_accel_data();
      break;
  
    case 5:
      // get_accel_data();
      get_commands();
      break;
  
    case 6:
      // get_accel_data();
      map_throttle();
      break;
      
    case 7:
      // get_accel_data();
      send_commands();
      break;
  
    case 8:
      // get_accel_data();
      break;

    case 9:
      // get_accel_data();
      break;
  }
  
  // loop timing
  delayMicroseconds(10000 - (micros() - time));
  //Serial.println(micros() - time);
}






