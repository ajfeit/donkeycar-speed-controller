// modules

// frames: 0
void debug_putput() {
  Serial.print(pwm1);
  Serial.print(" , ");
  Serial.print(pwm2);
  Serial.print(" , ");
  Serial.print(myIMU.az);
  Serial.print(" , ");
  Serial.print(speed);
  Serial.println();
}

// frames: 0,5
void get_commands() {
  // read serial commands
  cmd_since += 1;
  getSerialCmd(cmd);
}

// frames: 0,1,2,3,4,5,6,7,8,9
void get_accel_data() {
  myIMU.readAccelData(myIMU.accelCount);
  // myIMU.ax = (float)myIMU.accelCount[0] * myIMU.aRes; // - myIMU.accelBias[0];
  // myIMU.ay = (float)myIMU.accelCount[1] * myIMU.aRes; // - myIMU.accelBias[1];
  myIMU.az = (float)myIMU.accelCount[2] * myIMU.aRes; // - myIMU.accelBias[2];
  // myIMU.updateTime();
}

// frames: 1,6
void map_throttle() {
  // code to limit and filter motor and steering commands
  // ...
  // map throttle, 0 to 200 to PWM range
  thr_lf.proc(constrain(float(in1),0,200)/200.0);
  str_lf.proc((constrain(float(in2),0,200)/100.0) - 1.0); 
}

// frames: 2,7
void send_commands() {
  pwm1 = map(thr_lf.out()*200.0, 0, 200, THR_PWM_MIN, THR_PWM_MAX);
  pwm2 = map(str_lf.out()*100.0, -100, 100, STR_PWM_MIN, STR_PWM_MAX);
  // pwm.setPWM(0, 0, pwm1);
  // pwm.setPWM(1, 0, pwm2);
}

// frames: 3
void get_speed_data() {
  // handle maximum pulse length timeout
  if ((micros() - lastPulse) >= MAX_SPEED_PULSE) {
    lastPulseInterval = MAX_SPEED_PULSE;
    lastPulse = micros();
  }
  // get last pulse
  pulseSeconds = constrain(double(lastPulseInterval) / 1e6, 0.000001, 1.0);
  // gears: 7 pulses per drive-shaft revolution
  //  1.0 (??) wheel revolutions per drive-shaft rev
  // wheels:  0.2354 meters/rev
  outputValue = constrain( 7.0 / pulseSeconds, 0.0, 200.0) * 1.0 * 0.2354;
  speed = 0.95 * speedLast + 0.05 * outputValue;
  speedLast = speed;
}
