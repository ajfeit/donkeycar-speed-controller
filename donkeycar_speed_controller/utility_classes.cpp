#include "utility_classes.h"

LowPassFilter::LowPassFilter(float tau, float dt) {
  alpha = dt / (tau + dt);
}

void LowPassFilter::init() {
  y_m = 0;
}

void LowPassFilter::proc(float x) {
  y = (alpha * x) + ((1.0 - alpha) * y_m);
  y_m = y;
}

float LowPassFilter::out() {
  return y;
}

float LowPassFilter::get_alpha() {
  return alpha;
}
