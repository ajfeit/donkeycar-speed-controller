#ifndef UTILITY_CLASSES_H
#define UTILITY_CLASSES_H

#include <Arduino.h>

class LowPassFilter {
 private:
  float y;
  float y_m;
  float alpha;
  
 public:
  LowPassFilter(float tau, float dt);
  void init();
  void proc(float x);
  float out();
  float get_alpha();
};

#endif
